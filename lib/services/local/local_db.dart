import 'dart:io';

import 'package:fimber/fimber.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:weather_forecast2/model/CityModel.dart';

class LocalDatabase {
  static late Database db;
  static final String dbName = 'weather_forecast_DB';
  static final String tableName = 'city_table';
  static final String columnId = 'id';
  static final String columnName = 'name';
  static final String columnLat = 'lat';
  static final String columnLong = 'long';

  static Future open() async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, dbName);
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
      create table $tableName ( 
        $columnId integer primary key autoincrement, 
        $columnName text not null,
        $columnLong REAL not null,
        $columnLat REAL not null,
        UNIQUE($columnLong, $columnLat)
        );
      ''');
    });
  }

  static Future<List<CityModel>> getStoredCityList() async {
    List<Map<String, Object?>> maps = await db.query(tableName);
    if (maps.length > 0) {
      return List.generate(maps.length, (i) {
        return CityModel.fromMap(maps[i]);
      });
    }

    return [];
  }

  static Future<void> insert(String name, double long, double lat) async {
    await db.insert(tableName, CityModel.toMap(name, long, lat));
  }

  static Future close() async => db.close();
}

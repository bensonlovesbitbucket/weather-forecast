class APIEndpoint {
  static const String API_KEY = "appid=b08b66b15a8b49b44b060928f9f0cc4e";
  static const String CURRENT_WEATHER = "weather?";
  static const String FORECAST_WEATHER = "forecast?";
  static const String CELSIUS_UNIT = "&units=metric";
  static const String BASE_URL = "https://api.openweathermap.org/data/2.5/";
  static const String ICON_URL = "https://openweathermap.org/img/wn/";
  static const String REVERSE_GEOCODE_URL =
      "https://api.openweathermap.org/geo/1.0/reverse?";
}

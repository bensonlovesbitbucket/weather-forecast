import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:weather_forecast2/config/constant.dart';
import 'package:weather_forecast2/helper/error_exceptions.dart';
import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/model/CurrentWeatherModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherListModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherModel.dart';
import 'package:weather_forecast2/model/ReverseGeoCodeListModel.dart';
import 'package:weather_forecast2/model/TemperatureModel.dart';
import 'package:weather_forecast2/model/WeatherModel.dart';
import 'package:weather_forecast2/services/http/api_endpoint.dart';
import 'package:weather_forecast2/services/http/http_logging.dart';

class HttpClient {
  Dio _dio = Dio();

  HttpClient() {
    _dio
      ..options.baseUrl = APIEndpoint.BASE_URL
      ..options.connectTimeout = kDefaultConnectTimeout
      ..options.receiveTimeout = kDefaultReceiveTimeout
      ..httpClientAdapter
      ..options.headers = {'Content-Type': 'application/json; charset=UTF-8'};
    if (kDebugMode) {
      _dio.interceptors.add(LoggingInterceptor());
    }
  }

  Future<List<TemperatureModel>> getCityListWeather(
      List<CityModel> cityList) async {
    try {
      List<TemperatureModel> temperatureModelList = [];
      Future.forEach(cityList, (city) async {
        city as CityModel;
        try {
          final response = await _dio.get(APIEndpoint.BASE_URL +
              APIEndpoint.CURRENT_WEATHER +
              "q=" +
              city.name.toString());
          temperatureModelList.add(
              TemperatureModel.fromJson(response.data as Map<String, Object>));
        } catch (e) {}
      });
      return temperatureModelList;
    } catch (e) {
      throw ErrorExceptions.handleError(e);
    }
  }

  Future<CurrentWeatherModel> getCurrentWeather(String lat, String long) async {
    try {
      final response = await _dio.get(APIEndpoint.BASE_URL +
          APIEndpoint.CURRENT_WEATHER +
          "lat=" +
          lat +
          "&lon=" +
          long +
          "&" +
          APIEndpoint.API_KEY +
          APIEndpoint.CELSIUS_UNIT);
      return CurrentWeatherModel.fromJson(
          response.data as Map<String, dynamic>);
    } catch (e) {
      throw ErrorExceptions.handleError(e);
    }
  }

  Future<ForecastWeatherListModel> getForecastWeather(
      String lat, String long) async {
    try {
      final response = await _dio.get(APIEndpoint.BASE_URL +
          APIEndpoint.FORECAST_WEATHER +
          "lat=" +
          lat +
          "&lon=" +
          long +
          "&" +
          APIEndpoint.API_KEY +
          APIEndpoint.CELSIUS_UNIT);
      return ForecastWeatherListModel.fromJson(
          response.data as Map<String, dynamic>);
    } catch (e) {
      throw ErrorExceptions.handleError(e);
    }
  }

  Future<ReverseGeoCodeListModel> getReverseGeoCode(
      String lat, String long) async {
    try {
      final response = await _dio.get(APIEndpoint.REVERSE_GEOCODE_URL +
          "lat=" +
          lat +
          "&lon=" +
          long +
          "&" +
          APIEndpoint.API_KEY);
      return ReverseGeoCodeListModel.fromJson(
          response.data as Map<String, dynamic>);
    } catch (e) {
      throw ErrorExceptions.handleError(e);
    }
  }
}

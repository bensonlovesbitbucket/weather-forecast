import 'package:fimber/fimber.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/helper/error_exceptions.dart';
import 'package:weather_forecast2/repository/repository.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_event.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_state.dart';

class WeatherDetailsBloc extends Bloc<WeatherDetailsEvent, BaseState> {
  WeatherDetailsBloc(BaseState initialState) : super(initialState);

  @override
  Stream<BaseState> mapEventToState(WeatherDetailsEvent event) async* {
    try {
      if (event is GetWeatherDetails) {
        yield GetWeatherLoading();
        final response = await Repository()
            .getWeather(event.lat.toString(), event.long.toString());
        yield GetWeatherSuccess(response);
      } else if (event is GetForecastWeather) {
        yield GetForecastWeatherLoading();
        final response = await Repository()
            .getForecastWeather(event.lat.toString(), event.long.toString());
        yield GetForecastWeatherSuccess(response);
      }
    } catch (e) {
      if (event is GetForecastWeather) {
        yield GetForecastWeatherError(ErrorExceptions.handleError(e));
      } else if (event is GetWeatherDetails) {
        yield GetWeatherError(ErrorExceptions.handleError(e));
      }
    }
  }
}

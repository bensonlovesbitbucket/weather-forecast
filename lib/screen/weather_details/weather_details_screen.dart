import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/config/constant.dart';
import 'package:weather_forecast2/helper/ui_helper.dart';
import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/model/CurrentWeatherModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherListModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherModel.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_bloc.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_event.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_state.dart';
import 'package:weather_forecast2/services/http/api_endpoint.dart';
import 'package:weather_forecast2/theme/color.dart';

class WeatherDetailsScreen extends StatefulWidget {
  final int cityIndex;
  final List<CityModel> cityModelList;

  final double lat;
  final double long;

  const WeatherDetailsScreen(
      this.cityIndex, this.cityModelList, this.lat, this.long)
      : super();

  @override
  _WeatherDetailsScreenState createState() => _WeatherDetailsScreenState();
}

class _WeatherDetailsScreenState extends State<WeatherDetailsScreen> {
  late WeatherDetailsBloc _weatherDetailsBloc;
  late CurrentWeatherModel _currentWeatherModel;
  late ForecastWeatherListModel _forecastWeatherListModel;
  bool _forecastWeatherModelInitialized = false;
  bool _weatherModelInitialized = false;
  late int currentIndex;
  late int newIndex;

  @override
  void initState() {
    super.initState();
    _weatherDetailsBloc = BlocProvider.of<WeatherDetailsBloc>(context);
    _weatherDetailsBloc.add(GetWeatherDetails(widget.long, widget.lat));
    _weatherDetailsBloc.add(GetForecastWeather(widget.long, widget.lat));
    newIndex = currentIndex = widget.cityIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(kAppName),
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: BlocConsumer<WeatherDetailsBloc, BaseState>(
            listener: (context, state) {
          if (state is GetWeatherLoading) {
            setState(() {
              _weatherModelInitialized = false;
            });
          } else if (state is GetForecastWeatherLoading) {
            UIHelper.showLoadingDialog(context);
            setState(() {
              _forecastWeatherModelInitialized = false;
            });
          } else if (state is GetWeatherSuccess) {
            setState(() {
              currentIndex = newIndex;
              _currentWeatherModel = state.response;
              _weatherModelInitialized = true;
            });
          } else if (state is GetForecastWeatherSuccess) {
            Navigator.pop(context);
            setState(() {
              currentIndex = newIndex;
              _forecastWeatherModelInitialized = true;
              _forecastWeatherListModel = state.response;
            });
          } else if (state is GetWeatherError) {
          } else if (state is GetForecastWeatherError) {
            Navigator.pop(context);
            setState(() {
              _forecastWeatherModelInitialized = false;
              _weatherModelInitialized = false;
            });
          }
        }, builder: (context, state) {
          return Column(
            children: [
              Expanded(
                flex: 10,
                child: Center(
                    child: (() {
                  if (_weatherModelInitialized) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          _currentWeatherModel.name ?? "",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                        Flexible(
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 18.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Visibility(
                                    visible: ((currentIndex != -1) &&
                                        (currentIndex >= 1)),
                                    child: GestureDetector(
                                      onTap: () {
                                        newIndex = currentIndex - 1;
                                        _weatherDetailsBloc.add(
                                            GetWeatherDetails(
                                                widget
                                                    .cityModelList[
                                                        currentIndex - 1]
                                                    .long!,
                                                widget
                                                    .cityModelList[
                                                        currentIndex - 1]
                                                    .lat!));
                                        _weatherDetailsBloc.add(
                                            GetForecastWeather(
                                                widget
                                                    .cityModelList[
                                                        currentIndex - 1]
                                                    .long!,
                                                widget
                                                    .cityModelList[
                                                        currentIndex - 1]
                                                    .lat!));
                                      },
                                      child: Icon(
                                        Icons.arrow_back_rounded,
                                        color: kSecondaryColor,
                                        size: 36.0,
                                      ),
                                    )),

                                // (() {
                                //   if ((currentIndex != -1) &&
                                //       (currentIndex >= 1)) {
                                //     return
                                //      GestureDetector(
                                //       onTap: () {
                                //         _weatherDetailsBloc.add(
                                //             GetWeatherDetails(
                                //                 widget
                                //                     .cityModelList[
                                //                         currentIndex - 1]
                                //                     .long!,
                                //                 widget
                                //                     .cityModelList[
                                //                         currentIndex - 1]
                                //                     .lat!));
                                //         _weatherDetailsBloc.add(
                                //             GetForecastWeather(
                                //                 widget
                                //                     .cityModelList[
                                //                         currentIndex - 1]
                                //                     .long!,
                                //                 widget
                                //                     .cityModelList[
                                //                         currentIndex - 1]
                                //                     .lat!));
                                //       },
                                //       child: Icon(
                                //         Icons.arrow_back_rounded,
                                //         color: kSecondaryColor,
                                //         size: 36.0,
                                //       ),
                                //     );
                                //   } else {
                                //     return SizedBox.shrink();
                                //   }
                                // }()),
                                Image.network(
                                  _currentWeatherModel.weather[0] == null
                                      ? ""
                                      : APIEndpoint.ICON_URL +
                                          _currentWeatherModel.weather[0]!.icon
                                              .toString() +
                                          "@4x.png",
                                ),
                                Visibility(
                                    visible: ((currentIndex != -1) &&
                                        (currentIndex <
                                            (widget.cityModelList.length - 1))),
                                    child: GestureDetector(
                                      onTap: () {
                                        newIndex = currentIndex + 1;
                                        _weatherDetailsBloc.add(
                                            GetWeatherDetails(
                                                widget
                                                    .cityModelList[
                                                        currentIndex + 1]
                                                    .long!,
                                                widget
                                                    .cityModelList[
                                                        currentIndex + 1]
                                                    .lat!));
                                        _weatherDetailsBloc.add(
                                            GetForecastWeather(
                                                widget
                                                    .cityModelList[
                                                        currentIndex + 1]
                                                    .long!,
                                                widget
                                                    .cityModelList[
                                                        currentIndex + 1]
                                                    .lat!));
                                      },
                                      child: Icon(
                                        Icons.arrow_forward_rounded,
                                        color: kSecondaryColor,
                                        size: 36.0,
                                      ),
                                    )),

                                // (() {
                                //   if ((currentIndex != -1) &&
                                //       (currentIndex <
                                //           widget.cityModelList.length)) {
                                //     return Icon(
                                //       Icons.arrow_forward_rounded,
                                //       color: kSecondaryColor,
                                //       size: 36.0,
                                //     );
                                //   } else {
                                //     return SizedBox.shrink();
                                //   }
                                // }()),
                              ],
                            ),
                          ),
                        ),
                        Text(
                          _currentWeatherModel.main == null
                              ? ""
                              : _currentWeatherModel.main!.temp.toString(),
                          style: TextStyle(
                              fontSize: 23, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          _currentWeatherModel.weather[0] == null
                              ? ""
                              : _currentWeatherModel.weather[0]!.main!,
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    );
                  } else {
                    return Text("");
                  }
                }())),
              ),
              Expanded(
                flex: 3,
                child: (() {
                  if (_forecastWeatherModelInitialized) {
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: _forecastWeatherListModel.list.length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: 200,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    _forecastWeatherListModel
                                        .list[index].dt_txt,
                                    style: TextStyle(
                                        color: kPrimaryColor, fontSize: 18),
                                    textAlign: TextAlign.center),
                                Expanded(
                                  child: Image.network(
                                    APIEndpoint.ICON_URL +
                                        _forecastWeatherListModel
                                            .list[index].weather[0].icon! +
                                        "@2x.png",
                                  ),
                                ),
                                Text(
                                    _forecastWeatherListModel
                                            .list[index].main.temp
                                            .toString() +
                                        " \u2103",
                                    style: TextStyle(
                                        color: kPrimaryColor, fontSize: 20),
                                    textAlign: TextAlign.center),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  } else {
                    return Center(child: Text(""));
                  }
                }()),
              )
            ],
          );
        }),
      ),
    );
  }
}

import 'package:weather_forecast2/bloc/state.dart';

class GetWeatherLoading extends BaseState {}

class GetWeatherError extends BaseState {
  final String message;
  GetWeatherError(this.message);
}

class GetWeatherSuccess extends BaseState {
  final dynamic response;
  GetWeatherSuccess(this.response);
}

class GetForecastWeatherLoading extends BaseState {}

class GetForecastWeatherError extends BaseState {
  final String message;
  GetForecastWeatherError(this.message);
}

class GetForecastWeatherSuccess extends BaseState {
  final dynamic response;
  GetForecastWeatherSuccess(this.response);
}

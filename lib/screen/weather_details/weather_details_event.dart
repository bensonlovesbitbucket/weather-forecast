abstract class WeatherDetailsEvent {}

class GetWeatherDetails extends WeatherDetailsEvent {
  final double long;
  final double lat;
  GetWeatherDetails(this.long, this.lat);
}

class GetForecastWeather extends WeatherDetailsEvent {
  final double long;
  final double lat;
  GetForecastWeather(this.long, this.lat);
}

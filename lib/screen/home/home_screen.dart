import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/config/constant.dart';
import 'package:weather_forecast2/helper/location_helper.dart';
import 'package:weather_forecast2/helper/ui_helper.dart';
import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/repository/repository.dart';
import 'package:weather_forecast2/screen/home/home_bloc.dart';
import 'package:weather_forecast2/screen/home/home_event.dart';
import 'package:weather_forecast2/screen/home/home_state.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_screen.dart';
import 'package:weather_forecast2/theme/color.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  late HomeBloc _homeBloc;
  List<CityModel> cityModelList = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _openDB();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  void dispose() {
    Repository().closeDB();
    super.dispose();
  }

  _isLocationEnabled() async {
    UIHelper.showLoadingDialog(context);
    LocationHelper.ableToAccessLocationService().then((value) async {
      Navigator.pop(context);
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WeatherDetailsScreen(
                -1, cityModelList, position.latitude, position.longitude),
          ));
    }).catchError((e) {
      Navigator.pop(context);
      UIHelper.showToast(context, e.toString());
    });
  }

  _openDB() async {
    await Repository().openDB().then((value) {
      _homeBloc.add(GetStoredCityList());
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(kAppName),
      ),
      body: SafeArea(
        bottom: false,
        child: IndexedStack(
          children: <Widget>[
            buildCityList(),
            buildRoundedButton(),
            BlocConsumer<HomeBloc, BaseState>(builder: (context, state) {
              return buildStoredCityList();
            }, listener: (context, state) {
              if (state is GetStoredCityListLoading) {
              } else if (state is GetStoredCityListSuccess) {
                setState(() {
                  cityModelList = state.response;
                });
              } else if (state is GetStoredCityListError) {
              } else if (state is StoreCityLoading) {
                UIHelper.showLoadingDialog(context);
              } else if (state is StoreCityError) {
                Navigator.pop(context);
              } else if (state is StoreCitySuccess) {
                Navigator.pop(context);
                _homeBloc.add(GetStoredCityList());
              }
            })
          ],
          index: _selectedIndex,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'City List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: 'Current',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.save),
            label: 'Stored City List',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: kPrimaryColor,
        onTap: _onItemTapped,
      ),
    );
  }

  Widget buildCityList() {
    return Container(
      child: ListView.builder(
        itemCount: CityModel.cityList.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WeatherDetailsScreen(
                      index,
                      CityModel.cityList,
                      CityModel.cityList[index].lat!,
                      CityModel.cityList[index].long!),
                )),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      CityModel.cityList[index].name.toString(),
                      style: TextStyle(fontSize: 20),
                    ),
                    GestureDetector(
                      onTap: () =>
                          _homeBloc.add(StoreCity(CityModel.cityList[index])),
                      child: Icon(
                        Icons.add,
                        color: kPrimaryColor,
                        size: 24.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildStoredCityList() {
    if (cityModelList.isEmpty) {
      return Center(
        child: Text("Empty City List"),
      );
    } else {
      return Container(
        child: ListView.builder(
          itemCount: cityModelList.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => WeatherDetailsScreen(
                        index,
                        cityModelList,
                        cityModelList[index].lat!,
                        cityModelList[index].long!),
                  )),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        cityModelList[index].name.toString(),
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      );
    }
  }

  Widget buildRoundedButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Center(
          child: GestureDetector(
            onTap: _isLocationEnabled,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: kPrimaryColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    )
                  ]),
              child: Padding(
                padding: const EdgeInsets.all(58.0),
                child: Text(
                  "Tap to get \ncurrent location\nweather",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

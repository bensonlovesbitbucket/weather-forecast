import 'package:weather_forecast2/model/CityModel.dart';

abstract class HomeEvent {}

class GetStoredCityList extends HomeEvent {
  GetStoredCityList();
}

class StoreCity extends HomeEvent {
  final CityModel cityModel;
  StoreCity(this.cityModel);
}

class GetReverseGeoCode extends HomeEvent {
  final double lat;
  final double long;
  GetReverseGeoCode(this.lat, this.long);
}

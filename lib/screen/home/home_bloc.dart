import 'package:fimber/fimber.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/helper/error_exceptions.dart';
import 'package:weather_forecast2/repository/repository.dart';
import 'package:weather_forecast2/screen/home/home_event.dart';
import 'package:weather_forecast2/screen/home/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, BaseState> {
  HomeBloc(BaseState initialState) : super(initialState);

  @override
  Stream<BaseState> mapEventToState(HomeEvent event) async* {
    try {
      if (event is GetStoredCityList) {
        yield GetStoredCityListLoading();
        final response = await Repository().getStoredCityList();
        yield GetStoredCityListSuccess(response);
      } else if (event is StoreCity) {
        yield StoreCityLoading();
        await Repository().insertCity(
            event.cityModel.name!, event.cityModel.long!, event.cityModel.lat!);
        yield StoreCitySuccess();
      } else if (event is GetReverseGeoCode) {
        yield GetReverseGeoCodeLoading();
        final response = await Repository()
            .getReverseGeoCode(event.lat.toString(), event.long.toString());
        yield GetReverseGeoCodeSuccess(response);
      }
    } catch (e) {
      if (event is GetStoredCityList) {
        yield GetStoredCityListError(ErrorExceptions.handleError(e));
      } else if (event is StoreCity) {
        yield StoreCityError(ErrorExceptions.handleError(e));
      } else if (event is GetReverseGeoCode) {
        yield GetReverseGeoCodeError(ErrorExceptions.handleError(e));
      }
    }
  }
}

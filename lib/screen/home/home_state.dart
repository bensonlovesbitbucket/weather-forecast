import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/model/ReverseGeoCodeListModel.dart';

class GetStoredCityListLoading extends BaseState {}

class GetStoredCityListSuccess extends BaseState {
  final List<CityModel> response;
  GetStoredCityListSuccess(this.response);
}

class GetStoredCityListError extends BaseState {
  final String message;
  GetStoredCityListError(this.message);
}

class StoreCityLoading extends BaseState {}

class StoreCitySuccess extends BaseState {
  StoreCitySuccess();
}

class StoreCityError extends BaseState {
  final String message;
  StoreCityError(this.message);
}

class GetReverseGeoCodeLoading extends BaseState {}

class GetReverseGeoCodeSuccess extends BaseState {
  final ReverseGeoCodeListModel reverseGeoCodeListModel;
  GetReverseGeoCodeSuccess(this.reverseGeoCodeListModel);
}

class GetReverseGeoCodeError extends BaseState {
  final String message;
  GetReverseGeoCodeError(this.message);
}

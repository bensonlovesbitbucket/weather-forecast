// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ReverseGeoCodeListModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReverseGeoCodeListModel _$ReverseGeoCodeListModelFromJson(
    Map<String, dynamic> json) {
  return ReverseGeoCodeListModel(
    (json['reverseGeoCodeModelList'] as List<dynamic>)
        .map((e) => ReverseGeoCodeModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ReverseGeoCodeListModelToJson(
        ReverseGeoCodeListModel instance) =>
    <String, dynamic>{
      'reverseGeoCodeModelList':
          instance.reverseGeoCodeModelList.map((e) => e.toJson()).toList(),
    };

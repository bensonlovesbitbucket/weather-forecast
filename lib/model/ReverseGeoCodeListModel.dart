import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast2/model/ReverseGeoCodeModel.dart';

part 'ReverseGeoCodeListModel.g.dart';

@JsonSerializable(explicitToJson: true)
class ReverseGeoCodeListModel {
  List<ReverseGeoCodeModel> reverseGeoCodeModelList;

  ReverseGeoCodeListModel(this.reverseGeoCodeModelList);

  factory ReverseGeoCodeListModel.fromJson(Map<String, dynamic> json) =>
      _$ReverseGeoCodeListModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReverseGeoCodeListModelToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TemperatureModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemperatureModel _$TemperatureModelFromJson(Map<String, dynamic> json) {
  return TemperatureModel(
    (json['temp'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$TemperatureModelToJson(TemperatureModel instance) =>
    <String, dynamic>{
      'temp': instance.temp,
    };

import 'package:weather_forecast2/model/TemperatureModel.dart';
import 'package:weather_forecast2/model/WeatherModel.dart';
import 'package:json_annotation/json_annotation.dart';

part 'CurrentWeatherModel.g.dart';

@JsonSerializable(explicitToJson: true)
class CurrentWeatherModel {
  TemperatureModel? main;
  List<WeatherModel?> weather;
  String? name;

  CurrentWeatherModel(this.main, this.weather, this.name);

  factory CurrentWeatherModel.fromJson(Map<String, dynamic> json) =>
      _$CurrentWeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentWeatherModelToJson(this);
}

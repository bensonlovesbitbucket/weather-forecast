import 'package:json_annotation/json_annotation.dart';

part 'ReverseGeoCodeModel.g.dart';

@JsonSerializable(explicitToJson: true)
class ReverseGeoCodeModel {
  String? name;

  ReverseGeoCodeModel(this.name);

  factory ReverseGeoCodeModel.fromJson(Map<String, dynamic> json) =>
      _$ReverseGeoCodeModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReverseGeoCodeModelToJson(this);
}

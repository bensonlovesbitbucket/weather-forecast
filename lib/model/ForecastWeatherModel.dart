import 'package:weather_forecast2/model/TemperatureModel.dart';
import 'package:weather_forecast2/model/WeatherModel.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ForecastWeatherModel.g.dart';

@JsonSerializable(explicitToJson: true)
class ForecastWeatherModel {
  TemperatureModel main;
  List<WeatherModel> weather;
  String dt_txt;

  ForecastWeatherModel(this.main, this.weather, this.dt_txt);

  factory ForecastWeatherModel.fromJson(Map<String, dynamic> json) =>
      _$ForecastWeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastWeatherModelToJson(this);
}

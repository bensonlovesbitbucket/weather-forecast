// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ReverseGeoCodeModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReverseGeoCodeModel _$ReverseGeoCodeModelFromJson(Map<String, dynamic> json) {
  return ReverseGeoCodeModel(
    json['name'] as String?,
  );
}

Map<String, dynamic> _$ReverseGeoCodeModelToJson(
        ReverseGeoCodeModel instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

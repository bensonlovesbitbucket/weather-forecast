// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ForecastWeatherListModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForecastWeatherListModel _$ForecastWeatherListModelFromJson(
    Map<String, dynamic> json) {
  return ForecastWeatherListModel(
    (json['list'] as List<dynamic>)
        .map((e) => ForecastWeatherModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ForecastWeatherListModelToJson(
        ForecastWeatherListModel instance) =>
    <String, dynamic>{
      'list': instance.list.map((e) => e.toJson()).toList(),
    };

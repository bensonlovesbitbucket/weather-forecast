// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ForecastWeatherModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForecastWeatherModel _$ForecastWeatherModelFromJson(Map<String, dynamic> json) {
  return ForecastWeatherModel(
    TemperatureModel.fromJson(json['main'] as Map<String, dynamic>),
    (json['weather'] as List<dynamic>)
        .map((e) => WeatherModel.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['dt_txt'] as String,
  );
}

Map<String, dynamic> _$ForecastWeatherModelToJson(
        ForecastWeatherModel instance) =>
    <String, dynamic>{
      'main': instance.main.toJson(),
      'weather': instance.weather.map((e) => e.toJson()).toList(),
      'dt_txt': instance.dt_txt,
    };

import 'package:json_annotation/json_annotation.dart';

part 'TemperatureModel.g.dart';

@JsonSerializable(explicitToJson: true)
class TemperatureModel {
  double? temp;

  TemperatureModel(this.temp);

  factory TemperatureModel.fromJson(Map<String, dynamic> json) =>
      _$TemperatureModelFromJson(json);

  Map<String, dynamic> toJson() => _$TemperatureModelToJson(this);
}

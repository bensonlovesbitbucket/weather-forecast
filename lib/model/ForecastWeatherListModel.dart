import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast2/model/ForecastWeatherModel.dart';

part 'ForecastWeatherListModel.g.dart';

@JsonSerializable(explicitToJson: true)
class ForecastWeatherListModel {
  List<ForecastWeatherModel> list;

  ForecastWeatherListModel(this.list);

  factory ForecastWeatherListModel.fromJson(Map<String, dynamic> json) =>
      _$ForecastWeatherListModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastWeatherListModelToJson(this);
}

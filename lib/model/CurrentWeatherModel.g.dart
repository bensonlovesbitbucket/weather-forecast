// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CurrentWeatherModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentWeatherModel _$CurrentWeatherModelFromJson(Map<String, dynamic> json) {
  return CurrentWeatherModel(
    json['main'] == null
        ? null
        : TemperatureModel.fromJson(json['main'] as Map<String, dynamic>),
    (json['weather'] as List<dynamic>)
        .map((e) =>
            e == null ? null : WeatherModel.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['name'] as String?,
  );
}

Map<String, dynamic> _$CurrentWeatherModelToJson(
        CurrentWeatherModel instance) =>
    <String, dynamic>{
      'main': instance.main?.toJson(),
      'weather': instance.weather.map((e) => e?.toJson()).toList(),
      'name': instance.name,
    };

import 'package:weather_forecast2/services/local/local_db.dart';

class CityModel {
  String? name;
  double? lat;
  double? long;
  CityModel(this.name, this.lat, this.long);

  static Map<String, Object?> toMap(String _name, double _long, double _lat) {
    var map = <String, Object?>{
      LocalDatabase.columnName: _name,
      LocalDatabase.columnLong: _long,
      LocalDatabase.columnLat: _lat
    };
    return map;
  }

  CityModel.fromMap(Map<String, Object?> map) {
    name = map[LocalDatabase.columnName] as String;
    lat = map[LocalDatabase.columnLat] as double;
    long = map[LocalDatabase.columnLong] as double;
  }

  static List<CityModel> cityList = [
    CityModel("Kuala Lumpur", 3.1478, 101.6953),
    CityModel("Klang", 3.0333, 101.4500),
    CityModel("Ipoh", 4.6000, 101.0700),
    CityModel("Butterworth", 5.3942, 100.3664),
    CityModel("Johor Bahru", 1.4556, 103.7611),
    CityModel("George Town", 5.4145, 100.3292),
    CityModel("Petaling Jaya", 3.1073, 101.6067),
    CityModel("Kuantan", 3.8167, 103.3333),
    CityModel("Shah Alam", 3.0833, 101.5333),
    CityModel("Kota Bharu", 6.1333, 102.2500),
    CityModel("Melaka", 2.1889, 102.2511),
    CityModel("Kota Kinabalu", 5.9750, 116.0725),
    CityModel("Seremban", 2.7297, 101.9381),
    CityModel("Sandakan", 5.8388, 118.1173),
    CityModel("Sungai Petani", 5.6500, 100.4800),
    CityModel("Kuching", 1.5397, 110.3542),
    CityModel("Kuala Terengganu", 5.3303, 103.1408),
    CityModel("Alor Setar", 6.1167, 100.3667),
    CityModel("Putrajaya", 2.9300, 101.6900),
    CityModel("Kangar", 6.4414, 100.1986),
    CityModel("Labuan", 5.2803, 115.2475),
    CityModel("Pasir Mas", 6.0493, 102.1399),
    CityModel("Tumpat", 6.2000, 102.1667),
    CityModel("Ketereh", 5.9570, 102.2482),
    CityModel("Kampung Lemal", 6.0302, 102.1413),
    CityModel("Pulai Chondong", 5.8713, 102.2318),
  ];

  static List<CityModel> storedCityList = [
    CityModel("Kuala Lumpur", 3.1478, 101.6953),
    CityModel("Klang", 3.0333, 101.4500),
    CityModel("Ipoh", 4.6000, 101.0700),
    CityModel("Butterworth", 5.3942, 100.3664),
    CityModel("Johor Bahru", 1.4556, 103.7611),
    CityModel("George Town", 5.4145, 100.3292),
    CityModel("Petaling Jaya", 3.1073, 101.6067),
    CityModel("Kuantan", 3.8167, 103.3333),
    CityModel("Shah Alam", 3.0833, 101.5333),
    CityModel("Kota Bharu", 6.1333, 102.2500),
  ];
}

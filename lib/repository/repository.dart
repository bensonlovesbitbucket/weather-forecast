import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/model/CurrentWeatherModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherListModel.dart';
import 'package:weather_forecast2/model/ForecastWeatherModel.dart';
import 'package:weather_forecast2/model/ReverseGeoCodeListModel.dart';
import 'package:weather_forecast2/model/TemperatureModel.dart';
import 'package:weather_forecast2/services/http/http.dart';
import 'package:weather_forecast2/services/local/local_db.dart';

class Repository {
  static final Repository _repository = Repository._internal();

  factory Repository() {
    return _repository;
  }

  Repository._internal();

  Future<void> openDB() async {
    await LocalDatabase.open();
  }

  Future<void> closeDB() async {
    await LocalDatabase.close();
  }

  Future<List<CityModel>> getStoredCityList() async {
    return await LocalDatabase.getStoredCityList();
  }

  Future<void> insertCity(String name, double long, double lat) async {
    await LocalDatabase.insert(name, long, lat);
  }

  Future<CurrentWeatherModel> getWeather(String lat, String long) async {
    return await HttpClient().getCurrentWeather(lat, long);
  }

  Future<ForecastWeatherListModel> getForecastWeather(
      String lat, String long) async {
    return await HttpClient().getForecastWeather(lat, long);
  }

  Future<ReverseGeoCodeListModel> getReverseGeoCode(
      String lat, String long) async {
    return await HttpClient().getReverseGeoCode(lat, long);
  }
}

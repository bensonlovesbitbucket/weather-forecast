import 'package:device_preview/device_preview.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_forecast2/bloc/multi_bloc_provider.dart';
import 'package:weather_forecast2/model/CityModel.dart';
import 'package:weather_forecast2/screen/home/home_screen.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_screen.dart';
import 'package:weather_forecast2/theme/color.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (false) {
    runApp(
      DevicePreview(
        enabled: kDebugMode,
        builder: (context) => MyApp(),
      ),
    );
  } else {
    runApp(MyApp());
  }

  var tree = DebugTree();
  tree.colorizeMap["D"] = ColorizeStyle(
      [AnsiStyle(AnsiSelection.foreground, color: AnsiColor.yellow)]);
  Fimber.plantTree(tree);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: blocProviders,
      child: MaterialApp(
          builder: DevicePreview.appBuilder,
          theme: ThemeData(primaryColor: kPrimaryColor),
          debugShowCheckedModeBanner: false,
          home: HomeScreen(),
          initialRoute: '/'),
    );
  }
}

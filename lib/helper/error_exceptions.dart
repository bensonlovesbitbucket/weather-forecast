import 'dart:io';
import 'package:dio/dio.dart';
import 'package:fimber/fimber.dart';

class ErrorExceptions {
  static String handleError(dynamic error) {
    String errorDescription = "";
    if (error is String) {
      errorDescription = error;
    } else if (error is TypeError) {
      errorDescription = "Type error";
    } else if (error is FormatException) {
      errorDescription = "Parsing JSON exception.";
    } else if (error is DioError) {
      if (error is SocketException || error.error is SocketException) {
        errorDescription =
            "Connection failed. Please ensure you have a stable internet connection.";
      } else {
        DioError dioError = error;
        switch (dioError.type) {
          case DioErrorType.cancel:
            errorDescription = "Request was cancelled.";
            break;
          case DioErrorType.connectTimeout:
            errorDescription =
                "Connection timeout. Please ensure you have a stable internet connection.";
            break;
          case DioErrorType.other:
            errorDescription = "Network Error.";
            break;
          case DioErrorType.receiveTimeout:
            errorDescription =
                "Receive timeout. Please ensure you have a stable internet connection.";
            break;
          case DioErrorType.response:
            if (error.response?.data["message"] != null) {
              errorDescription = error.response?.data["message"] as String;
            }
            break;
          case DioErrorType.sendTimeout:
            errorDescription =
                "Send timeout. Please ensure you have a stable internet connection.";
            break;
        }
      }
    } else {
      errorDescription = "Unexpected error occured.";
    }
    return errorDescription;
  }
}

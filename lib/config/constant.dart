const kDefaultConnectTimeout = Duration.millisecondsPerMinute;
const kDefaultReceiveTimeout = Duration.millisecondsPerMinute;
const kAppName = "Weather Forecast";

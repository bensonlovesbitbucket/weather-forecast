import 'package:flutter/material.dart';

const kPrimaryColor = const Color.fromARGB(255, 251, 189, 35);
const kSecondaryColor = Colors.black;
const kCardLowerPart = const Color.fromARGB(255, 230, 227, 209);

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_forecast2/bloc/state.dart';
import 'package:weather_forecast2/screen/home/home_bloc.dart';
import 'package:weather_forecast2/screen/weather_details/weather_details_bloc.dart';

final blocProviders = [
  BlocProvider<WeatherDetailsBloc>(
    create: (context) => WeatherDetailsBloc(Initialized()),
  ),
  BlocProvider<HomeBloc>(
    create: (context) => HomeBloc(Initialized()),
  )
];

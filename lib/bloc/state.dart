abstract class BaseState {}

class Initialized extends BaseState {}

class Authenticated extends BaseState {}

class UnAuthenticated extends BaseState {}

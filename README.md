# Weather Forecast

Get to know different cities weather and the weather at ur current location

## Getting Started

This project runs on Flutter 2.0.2

## Demo
!["Demo"](assets/gif/demo.gif)
#
## UI Explanation
!["Mock"](assets/images/mock.png)
#
## Architecture Diagram
!["Architecture Diagram"](assets/images/architecture_diagram.png)
#
## Used packages
1. Flutter BLoC - To manage state efficiently
2. Device preview - To ensure the UI looks and feels stay persist in different devices during development
3. Geolocator - To get device ucrrent location
4. Fimber - One of the best logging tool that i always use
5. SQLFlite - Persist data